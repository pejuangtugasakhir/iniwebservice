<?php
	error_reporting(0);
	include '../../conf/conn.php';

	$get_data = "SELECT DISTINCT j.id_jadwal, m.nim, j.tanggal_seminar, j.waktu_seminar, m.nama_mhs, jdl.judul_ta, j.status_kegiatan, 
				GROUP_CONCAT(DISTINCT d.nama_dosen ORDER BY j.status_kegiatan,j.tanggal_seminar DESC SEPARATOR ' & ' ) 
				as dosen_pembimbing
				FROM jadwal_seminar j, mahasiswa m, judul_ta jdl, kategori_dosen k, status_dopingmhs s, dosen d
				WHERE jdl.nim=j.nim AND j.nim=m.nim AND s.nim=m.nim
				AND s.id_kategoridosen=k.id_kategoridosen
				AND k.nip=d.nip
				AND k.kategori='Dosen Pembimbing'
				GROUP BY j.tanggal_seminar
				ORDER BY j.status_kegiatan,j.tanggal_seminar DESC ";
				
	$qur = $connection->query($get_data);

	while ($r = mysqli_fetch_assoc($qur)) {
		$msg[] = array("id_jadwal" => $r['id_jadwal'],
                                        "tanggal_seminar" => $r['tanggal_seminar'], 
					"waktu_seminar" => $r['waktu_seminar'],
                                        "nim" => $r['nim'],
					"nama_mhs" => $r['nama_mhs'], 
					"judul_ta" => $r['judul_ta'],
					"status_kegiatan" => $r['status_kegiatan'],
					"dosen_pembimbing" => $r['dosen_pembimbing']);
	}

	$json = $msg;

	header('content-type: application/json');
	echo json_encode($json);

	@mysql_close($conn);
?>