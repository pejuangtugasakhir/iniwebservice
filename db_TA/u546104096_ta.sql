
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 21 Des 2015 pada 03.58
-- Versi Server: 10.0.20-MariaDB
-- Versi PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `u546104096_ta`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidang_ta`
--

CREATE TABLE IF NOT EXISTS `bidang_ta` (
  `id_bidang` int(10) NOT NULL AUTO_INCREMENT,
  `nama_bidang` varchar(100) NOT NULL,
  PRIMARY KEY (`id_bidang`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `bidang_ta`
--

INSERT INTO `bidang_ta` (`id_bidang`, `nama_bidang`) VALUES
(1, 'Multimedia'),
(2, 'Knowledge Management dan Data Science'),
(3, 'Computer System');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bimbingan_report`
--

CREATE TABLE IF NOT EXISTS `bimbingan_report` (
  `id_bimbingan` int(10) NOT NULL AUTO_INCREMENT,
  `nim` varchar(20) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `tgl_bimbingan` date NOT NULL,
  `topik_diskusi` text NOT NULL,
  `hasil_bimbingan` varchar(225) NOT NULL,
  PRIMARY KEY (`id_bimbingan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data untuk tabel `bimbingan_report`
--

INSERT INTO `bimbingan_report` (`id_bimbingan`, `nim`, `nip`, `tgl_bimbingan`, `topik_diskusi`, `hasil_bimbingan`) VALUES
(1, '121402038', '198603032002121002', '2015-08-10', 'Pengajuan Judul dan konsultasi judul', 'pembuatan excecutive summary'),
(2, '121402022', '198603032002121028', '2015-09-03', 'perihal perkembangan judul', 'didapatkan metode yang lebih baik untuk judul TA'),
(3, '121402056', '198001102008011010', '2015-10-28', 'diskusi kemajuan judul', 'metode terbaik untuk judul TA'),
(35, '121402056', '198603032002121002', '2015-12-06', 'diskusi metode judul', 'metode terapan ter'),
(42, '121402038', '195912311998021001', '2015-12-11', 'judul tugas akhir', 'judul fix'),
(28, '121402022', '198603032002121002', '2015-12-02', 'metode tugas akhir', 'metode'),
(37, '121402022', '198603032002121002', '2015-12-15', '', 'b');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE IF NOT EXISTS `dosen` (
  `nip` varchar(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `nama_dosen` varchar(100) NOT NULL,
  `alamat_dosen` varchar(100) NOT NULL,
  `email_dosen` varchar(50) NOT NULL,
  PRIMARY KEY (`nip`),
  KEY `id_user` (`id_user`),
  KEY `idx_user` (`id_user`),
  KEY `id_user_2` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`nip`, `id_user`, `nama_dosen`, `alamat_dosen`, `email_dosen`) VALUES
('198603032002121028', 14, 'Mohammad Fadly Syahputra, B.Sc., M.Sc.IT', 'jln', 'fadly@gmail.com'),
('197901082012121002', 13, 'Baihaqi Siregar, S.Si.,M.T', 'Jln Perintis Kemerdekaan', 'baihaqi.siregar@gmail.com'),
('198001102008011010', 3, 'M. Anggia Muchtar, ST.,MM.IT', 'Jln Letda Sujono Gg. Musyawarah No. 23', 'anggiamuchtar@gmail.com'),
('198603032002121002', 2, 'Romi Fadillah Rahmat, B. Comp.Sc., M.Sc', 'JLn Jamin ginting', 'romifadillah@gmail.com'),
('196108171987011001', 15, 'Prof. Dr. Opim S Sitompul', '', ''),
('195912311998021001', 16, 'Drs. Sawaludin, M.IT', '', ''),
('197503042008011008', 17, 'Muhammad Safri Lubis, ST., M.Comp', '', ''),
('197908312009121002', 18, 'Dedy Arisandi, ST, M.Kom', '', ''),
('198209152012121002', 19, 'Dani Gunawan ST, MIT', 'Jln', ''),
('132456823', 20, 'Hj. Nurrahmadayeni, S.Kom, M.Kom', 'Jln Jamin Ginting Gg Haji Arif', 'nurrahmadayeni.11@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `exsum`
--

CREATE TABLE IF NOT EXISTS `exsum` (
  `id_exsum` int(20) NOT NULL AUTO_INCREMENT,
  `nim` varchar(20) NOT NULL,
  `id_bidang` int(10) NOT NULL,
  `judul_exsum` text NOT NULL,
  `rekomendasi_exsum` enum('mahasiswa','dosen','dan lain-lain') NOT NULL,
  `tanggal_masuk_exsum` date NOT NULL,
  `status` enum('perbaikan besar','perbaikan kecil','diterima','ditolak','tahap pengajuan') NOT NULL,
  `keterangan_exsum` text NOT NULL,
  PRIMARY KEY (`id_exsum`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

--
-- Dumping data untuk tabel `exsum`
--

INSERT INTO `exsum` (`id_exsum`, `nim`, `id_bidang`, `judul_exsum`, `rekomendasi_exsum`, `tanggal_masuk_exsum`, `status`, `keterangan_exsum`) VALUES
(1, '121402038', 1, 'augmented reality untuk analisis tubuh manusia', 'mahasiswa', '2015-08-11', 'diterima', 'judul sudah bagus tinggal pengaplikasiannya'),
(2, '121402022', 2, 'deteksi retina mata pada penyakit hipertensi', 'dosen', '2015-09-01', 'diterima', 'pelajari metode yang akan digunakan'),
(3, '121402056', 2, 'deteksi jumlah kendaraan umum yang melintasi rs.adam malik menggunakan neural network', 'mahasiswa', '2015-10-04', 'diterima', 'harus menggunakan 2 metode'),
(4, '121402066', 3, 'pengenalan gerak tubuh menggunakan neural network', 'mahasiswa', '2015-11-02', 'tahap pengajuan', ''),
(86, '121402066', 3, 'yj', 'mahasiswa', '2015-12-10', 'tahap pengajuan', ''),
(88, '121402008', 3, 'Prediksi Mortalitas Menggunakan Text Mining', 'mahasiswa', '2015-12-13', 'tahap pengajuan', ''),
(78, '121402016', 2, 'sistem pemilihan ukm', 'mahasiswa', '2015-12-08', 'tahap pengajuan', ''),
(89, '121402022', 1, 'convolutional neural network', 'dosen', '2015-12-15', 'tahap pengajuan', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_seminar`
--

CREATE TABLE IF NOT EXISTS `jadwal_seminar` (
  `id_jadwal` int(10) NOT NULL AUTO_INCREMENT,
  `nim` varchar(20) NOT NULL,
  `tanggal_seminar` varchar(20) NOT NULL,
  `waktu_seminar` varchar(10) NOT NULL,
  `status_kegiatan` enum('sempro','semhas','sidang') NOT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data untuk tabel `jadwal_seminar`
--

INSERT INTO `jadwal_seminar` (`id_jadwal`, `nim`, `tanggal_seminar`, `waktu_seminar`, `status_kegiatan`) VALUES
(1, '121402038', '2015-09-04', '04:08', 'sempro'),
(23, '121402056', '2015-12-06', '23:00', 'sempro'),
(24, '121402022', '2016-01-07', '11:00', 'sempro'),
(25, '121402008', '2015-12-21', '13:00', 'sempro'),
(26, '121402008', '2016-01-14', '13:00', 'semhas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `judul_ta`
--

CREATE TABLE IF NOT EXISTS `judul_ta` (
  `id_judulta` int(10) NOT NULL AUTO_INCREMENT,
  `nim` varchar(20) NOT NULL,
  `judul_ta` varchar(225) NOT NULL,
  `id_bidang` varchar(20) NOT NULL,
  PRIMARY KEY (`id_judulta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `judul_ta`
--

INSERT INTO `judul_ta` (`id_judulta`, `nim`, `judul_ta`, `id_bidang`) VALUES
(1, '121402038', 'augmented reality untuk analisis tubuh manusia', '1'),
(2, '121402022', 'deteksi retina mata pada penyakit hipertensi', '2'),
(3, '121402056', 'deteksi jumlah kendaraan umum yang melintasi rs.adam malik menggunakan neural network', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_dosen`
--

CREATE TABLE IF NOT EXISTS `kategori_dosen` (
  `id_kategoridosen` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`id_kategoridosen`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data untuk tabel `kategori_dosen`
--

INSERT INTO `kategori_dosen` (`id_kategoridosen`, `nip`, `kategori`) VALUES
(1, '198603032002121002', 'Koordinator TA'),
(2, '198603032002121002', 'Dosen Pembimbing'),
(3, '198603032002121002', 'Dosen Pembanding'),
(4, '198603032002121002', 'Dosen Biasa'),
(5, '198001102008011010', 'Dosen Pembanding'),
(6, '198001102008011010', 'Dosen Pembimbing'),
(7, '198001102008011010', 'Dosen Biasa'),
(8, '197901082012121002', 'Dosen Pembimbing'),
(9, '197901082012121002', 'Dosen Pembanding'),
(10, '197901082012121002', 'Dosen Biasa'),
(11, '198603032002121028', 'Dosen Pembimbing'),
(12, '198603032002121028', 'Dosen Pembanding'),
(13, '198603032002121028', 'Dosen Biasa'),
(14, '196108171987011001', 'Dosen Pembimbing'),
(15, '196108171987011001', 'Dosen Biasa'),
(16, '196108171987011001', 'Dosen Pembanding'),
(17, '195912311998021001', 'Dosen Biasa'),
(18, '195912311998021001', 'Dosen Pembimbing'),
(19, '195912311998021001', 'Dosen Pembanding'),
(20, '197503042008011008', 'Dosen Pembanding'),
(21, '197503042008011008', 'Dosen Biasa'),
(22, '197503042008011008', 'Dosen Pembimbing'),
(23, '197908312009121002', 'Dosen Biasa'),
(24, '197908312009121002', 'Dosen Pembanding'),
(25, '197908312009121002', 'Dosen Pembimbing'),
(26, '198209152012121002', 'Dosen Pembimbing'),
(27, '198209152012121002', 'Dosen Biasa'),
(28, '198209152012121002', 'Dosen Pembanding');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `nim` varchar(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `nama_mhs` varchar(100) NOT NULL,
  `jenis_kelamin` enum('Perempuan','Laki-Laki') NOT NULL,
  `alamat_mhs` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`nim`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `id_user`, `nama_mhs`, `jenis_kelamin`, `alamat_mhs`, `email`) VALUES
('121402038', 4, 'Endang Windarsih', 'Perempuan', 'Jln. Pringgan Kry.2 Gg.Cemara', '1214038ew@gmail.com'),
('121402022', 5, 'Nurrahmadayeni', 'Perempuan', 'jln jamin ginting gg haji arif no 2g', 'nurrahmadayeni.11@gmail.com'),
('121402066', 6, 'siti hazizah harahap', 'Perempuan', 'jln marendal', 'azulaziza@gmail.com'),
('121402056', 7, 'ranti ramadhiana', 'Perempuan', 'jln binjai km 10.8', '1214056rr@gmail.com'),
('121402008', 8, 'ramadan', 'Laki-Laki', 'Jl. Letda Sujono No. 414', '1214008ram@gmail.com'),
('121402086', 9, 'siti fatimah', 'Perempuan', 'jln binjai ujung', 'fat@gmail.com'),
('121402100', 10, 'rona', 'Perempuan', 'dr.mansyur', 'rona@gmail.com'),
('121402110', 11, 'nurchalisa', 'Perempuan', 'jln nnn', 'anggii@gmail.com'),
('121402016', 12, 'annisa', 'Perempuan', 'jlan helvetia no.12 medan', 'annisafaradina\n@gmail.com'),
('1214020024', 21, 'Muhammad Wardana', 'Laki-Laki', 'Jalan Gak Tau', 'wardana@gmail.com'),
('121402014', 16, 'Muhammad Riwandy', 'Laki-Laki', 'Jln Kisaran', 'wandy@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `id_nilai` int(20) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `n_sempro` int(20) NOT NULL,
  `n_semhas` int(20) NOT NULL,
  `n_sidang` int(20) NOT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `nip`, `nim`, `n_sempro`, `n_semhas`, `n_sidang`) VALUES
(1, '197901082012121002', '121402038', 70, 70, 70);

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_dopingmhs`
--

CREATE TABLE IF NOT EXISTS `status_dopingmhs` (
  `nim` varchar(20) NOT NULL,
  `id_kategoridosen` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `status_dopingmhs`
--

INSERT INTO `status_dopingmhs` (`nim`, `id_kategoridosen`) VALUES
('121402038', 2),
('121402038', 18),
('121402038', 9),
('121402056', 6),
('121402056', 2),
('121402056', 12),
('121402022', 2),
('121402022', 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('dosen','mahasiswa','koordinator TA','admin') NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(2, 'romi', '910b6c78a8482033b971116f02441ce4', 'koordinator TA'),
(3, 'anggi', '4a283e1f5eb8628c8631718fe87f5917', 'dosen'),
(4, 'hyunsie', '7565a1fb1ffd44d62ba851ce3540b5e4', 'mahasiswa'),
(5, 'yeni', '0fc37dce7e27a505363a2586f4483b92', 'mahasiswa'),
(6, 'ziza', '432e52725da49a59837658ae2c029f11', 'mahasiswa'),
(7, 'ranti', '78b26bc43ede55c1fe5c59a10c714c73', 'mahasiswa'),
(8, 'madan', '0257acd5bb44b1bda717c403f016faab', 'mahasiswa'),
(9, 'fatimah', '3610528e7fee68c6ffb0445603ef2c8e', 'mahasiswa'),
(10, 'rona', '689b6f533e39e77830b46315ab4cb501', 'mahasiswa'),
(11, 'nurchalisa', '827ccb0eea8a706c4c34a16891f84e7b', 'mahasiswa'),
(12, 'nisa', '5fad30428811fe378fd389cd7659a33c', 'mahasiswa'),
(13, 'baihaqi', 'd1652902023eb117cd6ddf04eddf11e0', 'dosen'),
(14, 'Saya', '827ccb0eea8a706c4c34a16891f84e7b', 'dosen'),
(15, 'seniman', 'ed07dd62f3a47384034f31e9abbdf336', 'dosen'),
(16, 'wandy', '8c42eb74416a43751076e754f1cf87e5', 'dosen'),
(17, 'ye', '00c66f1a036bd8f9cb709cb8d925d3d9', 'dosen'),
(18, '', 'd41d8cd98f00b204e9800998ecf8427e', 'dosen'),
(19, 'gge', '12345', 'dosen'),
(20, 'nur', 'b55178b011bfb206965f2638d0f87047', 'dosen'),
(21, 'wardana', '7bf2fdbe4e28bf4a7ea60e6d0e2af546', 'mahasiswa');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
