<?php
	error_reporting(0);
	include '../../conf/conn.php';

	$get_data = "SELECT DISTINCT e.judul_exsum as judul, b.nama_bidang as bidang, 
				m.nama_mhs, e.status 
				FROM exsum e, bidang_ta b, mahasiswa m
				WHERE e.nim=m.nim AND e.id_bidang=b.id_bidang
				AND e.status='diterima' 
				OR e.status='ditolak'";
				
	$qur = $connection->query($get_data);

	while ($r = mysqli_fetch_assoc($qur)) {
		$msg[] = array("judul" => $r['judul'], 
					"bidang" => $r['bidang'], 
					"nama_mhs" => $r['nama_mhs'], 
					"status" => $r['status']);
	}

	$json = $msg;

	header('content-type: application/json');
	echo json_encode($json);

	@mysql_close($conn);
?>