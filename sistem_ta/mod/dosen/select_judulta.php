<?php
	error_reporting(0);
	include '../../conf/conn.php';

	$get_data = "SELECT DISTINCT jdl.id_judulta, m.nim, m.nama_mhs, jdl.judul_ta, b.id_bidang, m.nama_mhs, b.nama_bidang
				FROM mahasiswa m, judul_ta jdl, bidang_ta b
				WHERE jdl.id_bidang=b.id_bidang AND jdl.nim=m.nim
				ORDER BY jdl.id_judulta DESC ";
				
	$qur = $connection->query($get_data);

	while ($r = mysqli_fetch_assoc($qur)) {
		$msg[] = array("id_judulta" => $r['id_judulta'],
                                        "nim" => $r['nim'],
                                        "nama_mhs" => $r['nama_mhs'],  
					"judul_ta" => $r['judul_ta'],
                                        "nama_bidang" => $r['nama_bidang']);
	}

	$json = $msg;

	header('content-type: application/json');
	echo json_encode($json);

	@mysql_close($conn);
?>